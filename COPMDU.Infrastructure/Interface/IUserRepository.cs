﻿using COPMDU.Domain;
using System.Collections.Generic;

namespace COPMDU.Infrastructure.Interface
{
    public interface IUserRepository
    {
        IEnumerable<User> GetAll();
        List<string> GetUsername(string partial);
        User Get(int id);
        void Add(User entity);
        void Update(User entity);
        void Delete(User entity);
        User Authenticate(Login login);
    }
}