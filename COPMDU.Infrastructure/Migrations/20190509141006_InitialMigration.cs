﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace COPMDU.Infrastructure.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExternalSystem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExternalSystem", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Log",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Source = table.Column<string>(nullable: true),
                    StackTrace = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    Ticket = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Log", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OutageSymptom",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutageSymptom", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OutageType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutageType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Resolution",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    AtlasId = table.Column<int>(nullable: false),
                    AtlasDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resolution", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Service",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Service", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ServiceOrderType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceOrderType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SignalStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SignalStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SignalTerminal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SignalTerminal", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Username",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Username", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WebCommLog",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Uri = table.Column<string>(nullable: true),
                    Path = table.Column<string>(nullable: true),
                    Param = table.Column<string>(nullable: true),
                    Body = table.Column<string>(nullable: true),
                    ResponseCode = table.Column<int>(nullable: false),
                    Method = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebCommLog", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "City",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    EnabledInteration = table.Column<bool>(nullable: false),
                    ExternalSystemId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_City", x => x.Id);
                    table.ForeignKey(
                        name: "FK_City_ExternalSystem_ExternalSystemId",
                        column: x => x.ExternalSystemId,
                        principalTable: "ExternalSystem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    CityId = table.Column<int>(nullable: true),
                    Prefix = table.Column<int>(nullable: false),
                    Phone = table.Column<int>(nullable: false),
                    Username = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ChangePassword = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_City_CityId",
                        column: x => x.CityId,
                        principalTable: "City",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Outage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ServiceOrderTypeId = table.Column<int>(nullable: true),
                    CityId = table.Column<int>(nullable: true),
                    ServiceOrderUpdatedTime = table.Column<DateTime>(nullable: false),
                    UpdatedTime = table.Column<DateTime>(nullable: false),
                    UpdatedDetail = table.Column<DateTime>(nullable: false),
                    TypeId = table.Column<int>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    SymptomId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Filtered = table.Column<bool>(nullable: false),
                    OpenedById = table.Column<int>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    PredictionDate = table.Column<DateTime>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    AddressComplement = table.Column<string>(nullable: true),
                    AddressComplement2 = table.Column<string>(nullable: true),
                    Node = table.Column<string>(nullable: true),
                    IdMdu = table.Column<int>(nullable: false),
                    TechnicUserId = table.Column<int>(nullable: true),
                    Contract = table.Column<long>(nullable: false),
                    Notification = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    ClosedById = table.Column<int>(nullable: true),
                    ValidSignal = table.Column<bool>(nullable: true),
                    SignalDate = table.Column<DateTime>(nullable: true),
                    SignalExpiration = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Outage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Outage_City_CityId",
                        column: x => x.CityId,
                        principalTable: "City",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Outage_User_ClosedById",
                        column: x => x.ClosedById,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Outage_Username_OpenedById",
                        column: x => x.OpenedById,
                        principalTable: "Username",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Outage_ServiceOrderType_ServiceOrderTypeId",
                        column: x => x.ServiceOrderTypeId,
                        principalTable: "ServiceOrderType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Outage_OutageSymptom_SymptomId",
                        column: x => x.SymptomId,
                        principalTable: "OutageSymptom",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Outage_User_TechnicUserId",
                        column: x => x.TechnicUserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Outage_OutageType_TypeId",
                        column: x => x.TypeId,
                        principalTable: "OutageType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OutageService",
                columns: table => new
                {
                    OutageId = table.Column<int>(nullable: false),
                    ServiceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutageService", x => new { x.OutageId, x.ServiceId });
                    table.ForeignKey(
                        name: "FK_OutageService_Outage_OutageId",
                        column: x => x.OutageId,
                        principalTable: "Outage",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OutageService_Service_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Service",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OutageSignal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    OutageId = table.Column<int>(nullable: false),
                    Contract = table.Column<int>(nullable: false),
                    TerminalId = table.Column<int>(nullable: true),
                    Mac = table.Column<string>(nullable: true),
                    StatusId = table.Column<int>(nullable: true),
                    ValidStatus = table.Column<bool>(nullable: false),
                    CmtsSnr = table.Column<float>(nullable: false),
                    ValidCmtsSnr = table.Column<bool>(nullable: false),
                    CmTx = table.Column<float>(nullable: false),
                    ValidCmTx = table.Column<bool>(nullable: false),
                    ValidCmRx = table.Column<bool>(nullable: false),
                    ValidCmSnr = table.Column<bool>(nullable: false),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OutageSignal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OutageSignal_Outage_OutageId",
                        column: x => x.OutageId,
                        principalTable: "Outage",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OutageSignal_SignalStatus_StatusId",
                        column: x => x.StatusId,
                        principalTable: "SignalStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OutageSignal_SignalTerminal_TerminalId",
                        column: x => x.TerminalId,
                        principalTable: "SignalTerminal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SignalCmRx",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Value = table.Column<float>(nullable: false),
                    Valid = table.Column<bool>(nullable: false),
                    OutageSignalId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SignalCmRx", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SignalCmRx_OutageSignal_OutageSignalId",
                        column: x => x.OutageSignalId,
                        principalTable: "OutageSignal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SignalCmSnr",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Value = table.Column<float>(nullable: false),
                    Valid = table.Column<bool>(nullable: false),
                    OutageSignalId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SignalCmSnr", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SignalCmSnr_OutageSignal_OutageSignalId",
                        column: x => x.OutageSignalId,
                        principalTable: "OutageSignal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "ExternalSystem",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "netsul" },
                    { 2, "sigabc" },
                    { 3, "sigmaisp" },
                    { 4, "sigsantos" },
                    { 5, "sigsoc" },
                    { 6, "sigspo" }
                });

            migrationBuilder.InsertData(
                table: "OutageSymptom",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 3, "SEM SINAL" },
                    { 4, "INTERMITENTE" },
                    { 1, "INFORMATIVO" },
                    { 2, "DEGRADAÇÃO" }
                });

            migrationBuilder.InsertData(
                table: "Resolution",
                columns: new[] { "Id", "AtlasDescription", "AtlasId", "Description" },
                values: new object[,]
                {
                    { 3063, "PASSIVO DE BACKBONE DANIFICADO", 7060, "PASSIVO DANIFICADO" },
                    { 5425, "REPASSADO PARA REDE EXTERNA", 7062, "REDE EXTERNA COM PROBLEMA" },
                    { 5424, "OCORRENCIA CANCELADA", 7070, "ERRO DE ABERTURA" },
                    { 5423, "PROBLEMA INTERMITENTE DE BACKBONE NAO CONSTATADO", 7059, "NORMALIZADO SEM INTERVENCAO" },
                    { 5406, "FURTO DE AMPLIFICADOR INDOOR", 7053, "FURTO DE ATIVO" },
                    { 5405, "AMPLIFICADOR INDOOR COM DEFEITO - EMERGÊNCIA", 7047, "BOOT NO AMPLIFICADOR" },
                    { 5404, "AMPLIFICADOR INDOOR COM DEFEITO - EMERGÊNCIA", 7047, "AMPLIFICADOR QUEIMADO" },
                    { 5403, "ALTERAÇÃO DE ALIMENTAÇÃO ELÉTRICA DO INDOOR", 7067, "DISJUNTOR DO MDU DESARMADO" },
                    { 3064, "REFEITA CONEXAO DO BACKBONE", 7051, "REFEITA CONEXÃO DO CABO RG11 NO TAP" },
                    { 3062, "ALIMENTACAO INTERNA(PROBLEMA DE AC)", 7046, "ENERGIA ELÉTRICA DO CONDOMÍNIO CORTADA PELA CONCESSIONÁRIA" },
                    { 3067, "OCORRENCIA CANCELADA", 7070, "ACESSO PROIBIDO AO PONTO DE FALHA" },
                    { 2968, "ROMPIMENTO DO CABO DE BACKBONE", 7045, "TROCA DE DROP" },
                    { 2967, "AMPLIFICADOR INDOOR COM DEFEITO - EMERGÊNCIA", 7047, "TROCA DE AMPLIFICADOR" },
                    { 2966, "ADEQUAÇÃO / CONSTRUÇÃO DE MDU", 7064, "RECONSTRUÇÃO DE MDU" },
                    { 2965, "REFEITA CONEXAO DO BACKBONE", 7051, "REFEITA CONEXÃO - BACK BONE" },
                    { 2964, "ALTERAÇÃO DE ALIMENTAÇÃO ELÉTRICA DO INDOOR", 7067, "QUEDA DE ENERGIA - FORNECIMENTO REESTABELECIDO" },
                    { 2963, "ALTERAÇÃO DE ALIMENTAÇÃO ELÉTRICA DO INDOOR", 7067, "QUEDA DE ENERGIA - ACIONADO GERADOR" },
                    { 2962, "ADEQUAÇÃO / CONSTRUÇÃO DE MDU", 7064, "MANUTENÇÃO NO DG" },
                    { 2961, "ADEQUAÇÃO / CONSTRUÇÃO DE MDU", 7064, "MANUTENÇÃO NO AMPLIFICADOR" },
                    { 2960, "LIMPEZA DE RUÍDO NO MDU", 7050, "LIMPEZA DE RUÍDO" },
                    { 2959, "EQUALIZACAO DO AMPLIFICADOR", 7048, "EQUALIZAÇÃO DE AMPLIFICADOR" },
                    { 3061, "ALTERAÇÃO DE ALIMENTAÇÃO ELÉTRICA DO INDOOR", 7067, "DISJUNTOR DE SERVIÇO DO CONDOMÍNIO DESLIGADO" }
                });

            migrationBuilder.InsertData(
                table: "Service",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 21, "TV Everywhere" },
                    { 20, "NOW Online" },
                    { 19, "AVV" },
                    { 18, "WiFi" },
                    { 17, "BSOD" },
                    { 6, "Pay TV - Analógico" },
                    { 4, "Pay TV - Digital" },
                    { 2, "NET FONE" },
                    { 1, "NET VIRTUA" },
                    { 16, "NOW" }
                });

            migrationBuilder.InsertData(
                table: "ServiceOrderType",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 114, "114" },
                    { 115, "115" }
                });

            migrationBuilder.InsertData(
                table: "SignalStatus",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Online" },
                    { 2, "Offline" },
                    { 3, "NOT FOUND" },
                    { 4, "RangingAutoAdjComplete" }
                });

            migrationBuilder.InsertData(
                table: "SignalTerminal",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "DECODER DIGITAL" },
                    { 2, "EMTA" }
                });

            migrationBuilder.InsertData(
                table: "City",
                columns: new[] { "Id", "EnabledInteration", "ExternalSystemId", "Name" },
                values: new object[,]
                {
                    { 233, false, 1, "Castanhal" },
                    { 5509, false, 3, "Campinas" },
                    { 5565, false, 3, "Cosmópolis" },
                    { 5623, false, 3, "Franca" },
                    { 5693, false, 3, "Indaiatuba" },
                    { 5733, false, 3, "Itaquaquecetuba" },
                    { 5776, false, 3, "Jundiaí" },
                    { 5954, false, 3, "Paulínia" },
                    { 5985, false, 3, "Piracicaba" },
                    { 6009, false, 3, "Poá" },
                    { 6036, false, 3, "Presidente Prudente" },
                    { 6067, false, 3, "Ribeirão Pires" },
                    { 6070, false, 3, "Ribeirão Preto" },
                    { 6144, false, 3, "São Carlos" },
                    { 6158, false, 3, "São José do Rio Preto" },
                    { 6209, false, 3, "Suzano" },
                    { 6294, true, 3, "Almirante Tamandaré" },
                    { 6333, true, 3, "Araucária" },
                    { 7603, true, 3, "Canoas" },
                    { 7584, true, 3, "Campo Bom" },
                    { 7363, true, 3, "São José" },
                    { 7278, true, 3, "Palhoça" },
                    { 7260, true, 3, "Navegantes" },
                    { 7194, true, 3, "Itapema" },
                    { 5449, false, 3, "Bauru" },
                    { 7193, true, 3, "Itajaí" },
                    { 7070, true, 3, "Brusque" },
                    { 7052, true, 3, "Balneário Camboriú" },
                    { 6923, true, 3, "São José dos Pinhais" },
                    { 6770, true, 3, "Pinhais" },
                    { 6437, true, 3, "Colombo" },
                    { 6389, true, 3, "Campo Largo" },
                    { 7154, false, 3, "Gaspar" },
                    { 5419, false, 3, "Artur Nogueira" },
                    { 5359, true, 3, "Volta Redonda" },
                    { 5338, true, 3, "Teresópolis" },
                    { 4126, true, 3, "Juiz de Fora" },
                    { 4060, true, 3, "Ituiutaba" },
                    { 4005, true, 3, "Ipatinga" },
                    { 3930, true, 3, "Governador Valadares" },
                    { 3832, false, 3, "Divinópolis" },
                    { 3753, true, 3, "Contagem" },
                    { 4143, false, 3, "Lagoa Santa" },
                    { 3741, true, 3, "Conselheiro Lafaiete" },
                    { 3455, false, 3, "Araguari" },
                    { 3363, true, 3, "Vitória da Conquista" },
                    { 2852, false, 3, "Feira de Santana" },
                    { 2750, false, 3, "Camaçari" },
                    { 2550, true, 3, "Aracajú" },
                    { 2152, true, 3, "Caruaru" },
                    { 3514, true, 3, "Betim" },
                    { 7757, true, 3, "Estância Velha" },
                    { 4275, false, 3, "Montes Claros" },
                    { 4541, true, 3, "Sabará" },
                    { 5305, true, 3, "São Gonçalo" },
                    { 5285, true, 3, "Rio das Ostras" },
                    { 5250, true, 3, "Petrópolis" },
                    { 5230, false, 3, "Nova Friburgo" },
                    { 5197, true, 3, "Macaé" },
                    { 5129, false, 3, "Campos dos Goytacazes" },
                    { 4312, true, 3, "Nova Lima" },
                    { 5066, true, 3, "Serra" },
                    { 4911, false, 3, "Cachoeiro de Itapemirim" },
                    { 4834, false, 3, "Vespasiano" },
                    { 4806, true, 3, "Uberlândia" },
                    { 4803, true, 3, "Uberaba" },
                    { 4763, true, 3, "Teófilo Otoni" },
                    { 4742, true, 3, "Sete Lagoas" },
                    { 4917, true, 3, "Cariacica" },
                    { 7758, true, 3, "Esteio" },
                    { 7820, true, 3, "Gravataí" },
                    { 8330, true, 3, "Sapiranga" },
                    { 5820, false, 5, "Marília" },
                    { 5795, false, 5, "Lorena" },
                    { 5769, false, 5, "Jaú" },
                    { 5765, false, 5, "Jandira" },
                    { 5722, false, 5, "Itapevi" },
                    { 5714, false, 5, "Itapecerica da Serra" },
                    { 5882, false, 5, "Monte Mor" },
                    { 5661, false, 5, "Guarulhos" },
                    { 5599, false, 5, "Embu das Artes" },
                    { 5595, false, 5, "Elias Fausto" },
                    { 5572, false, 5, "Cruzeiro" },
                    { 5567, false, 5, "Cotia" },
                    { 5533, false, 5, "Carapicuíba" },
                    { 5530, false, 5, "Capivari" },
                    { 5656, false, 5, "Guaratinguetá" },
                    { 5488, false, 5, "Cachoeira Paulista" },
                    { 5924, false, 5, "Osasco" },
                    { 6024, false, 5, "Potim" },
                    { 8719, true, 5, "Cuiabá" },
                    { 6794, true, 5, "Ponta Grossa" },
                    { 6531, true, 5, "Guarapuava" },
                    { 6430, true, 5, "Cianorte" },
                    { 6413, true, 5, "Cascavel" },
                    { 6276, false, 5, "Vargem Grande Paulista" },
                    { 6022, false, 5, "Porto Feliz" },
                    { 6272, false, 5, "Valinhos" },
                    { 6216, false, 5, "Taboão da Serra" },
                    { 6201, false, 5, "Sorocaba" },
                    { 6195, false, 5, "Sertãozinho" },
                    { 6121, false, 5, "Santana do Parnaíba" },
                    { 6095, false, 5, "Salto" },
                    { 6048, false, 5, "Rafard" },
                    { 6241, false, 5, "Tietê" },
                    { 1847, true, 3, "Campina Grande" },
                    { 5471, false, 5, "Botucatu" },
                    { 5399, false, 5, "Aparecida" },
                    { 178, true, 5, "Ananindeua" },
                    { 63118, true, 4, "Rio de Janeiro" },
                    { 25666, true, 4, "Belo Horizonte" },
                    { 19887, true, 4, "Goiânia" },
                    { 18511, true, 4, "Anápolis" },
                    { 15890, true, 4, "Brasília" },
                    { 194, true, 5, "Belém" },
                    { 6139, false, 4, "Santos" },
                    { 89710, false, 3, "São Leopoldo" },
                    { 8895, true, 3, "Aparecida de Goiânia" },
                    { 8818, true, 3, "Rondonópolis" },
                    { 8567, true, 3, "Dourados" },
                    { 8533, true, 3, "Campo Grande" },
                    { 8332, true, 3, "Sapucaia do Sul" },
                    { 5228, true, 4, "Niterói" },
                    { 5443, false, 5, "Barueri" },
                    { 696, true, 5, "São Luís" },
                    { 1097, true, 5, "Fortaleza" },
                    { 5316, true, 5, "São João de Meriti" },
                    { 5239, true, 5, "Mesquita" },
                    { 5237, true, 5, "Nova Iguaçu" },
                    { 5226, true, 5, "Nilópolis" },
                    { 5161, true, 5, "Duque de Caxias" },
                    { 5105, true, 5, "Belford Roxo" },
                    { 863, true, 5, "Teresina" },
                    { 5083, true, 5, "Vitória" },
                    { 3230, true, 5, "Salvador" },
                    { 3042, true, 5, "Lauro de Freitas" },
                    { 2480, true, 5, "Maceió" },
                    { 1907, true, 5, "João Pessoa" },
                    { 1695, true, 5, "Natal" },
                    { 1637, true, 5, "Parnamirim" },
                    { 5078, true, 5, "Vila Velha" },
                    { 1833, true, 3, "Cabedelo" },
                    { 540, true, 3, "Palmas" },
                    { 66, true, 3, "Rio Branco" },
                    { 56995, true, 1, "Londrina" },
                    { 55298, false, 1, "Curitiba" },
                    { 53902, true, 1, "Arapongas" },
                    { 9112, false, 1, "Rio Verde" },
                    { 8838, false, 1, "Sorriso" },
                    { 8837, false, 1, "Sinop" },
                    { 57304, true, 1, "Maringá" },
                    { 8493, false, 1, "Xangri-lá" },
                    { 8398, false, 1, "Torres" },
                    { 7950, false, 1, "Montenegro" },
                    { 7817, false, 1, "Gramado" },
                    { 7614, false, 1, "Carazinho" },
                    { 7601, false, 1, "Canela" },
                    { 7560, true, 1, "Cachoeirinha" },
                    { 8471, false, 1, "Viamão" },
                    { 7458, false, 1, "Alvorada" },
                    { 66770, true, 1, "Bagé" },
                    { 67784, true, 1, "Capão da Canoa" },
                    { 75680, true, 1, "Blumenau" },
                    { 74748, true, 1, "Uruguaiana" },
                    { 72907, true, 1, "Santa Maria" },
                    { 72842, true, 1, "Santa Cruz do Sul" },
                    { 72451, true, 1, "Rio Grande" },
                    { 71986, true, 1, "Porto Alegre" },
                    { 67040, true, 1, "Bento Gonçalves" },
                    { 71706, true, 1, "Pelotas" },
                    { 71242, false, 1, "Novo Hamburgo" },
                    { 70408, true, 1, "Lajeado" },
                    { 69337, true, 1, "Farroupilha" },
                    { 69019, true, 1, "Erechim" },
                    { 68659, true, 1, "Cruz Alta" },
                    { 68020, true, 1, "Caxias do Sul" },
                    { 71587, true, 1, "Passo Fundo" },
                    { 75681, true, 1, "Blumenau Btv" },
                    { 7213, false, 1, "Lages" },
                    { 7163, false, 1, "Guaramirim" },
                    { 5498, false, 1, "Caieiras" },
                    { 5448, false, 1, "Batatais" },
                    { 5420, false, 1, "Arujá" },
                    { 5320, false, 1, "São Pedro da Aldeia" },
                    { 5115, false, 1, "Cabo Frio" },
                    { 4821, true, 1, "Varginha" },
                    { 5543, false, 1, "Catanduva" },
                    { 4780, false, 1, "Três Corações" },
                    { 4437, false, 1, "Poços de Caldas" },
                    { 4024, false, 1, "Itajubá" },
                    { 3778, false, 1, "Coronel Fabriciano" },
                    { 1521, false, 1, "Sobral" },
                    { 393, false, 1, "Macapa" },
                    { 272, false, 1, "Maraba" },
                    { 4459, false, 1, "Pouso Alegre" },
                    { 7203, false, 1, "Jaraguá do Sul" },
                    { 5586, false, 1, "Dracena" },
                    { 5798, false, 1, "Louveira" },
                    { 7058, false, 1, "Biguaçu" },
                    { 6851, false, 1, "Rolândia" },
                    { 6749, false, 1, "Paranaguá" },
                    { 6506, false, 1, "Foz do Iguaçu" },
                    { 6379, false, 1, "Cambé" },
                    { 6322, false, 1, "Apucarana" },
                    { 5739, false, 1, "Itatiba" },
                    { 6282, false, 1, "Votorantim" },
                    { 6277, false, 1, "Várzea Paulista" },
                    { 6246, false, 1, "Tremembé" },
                    { 6233, false, 1, "Tatuí" },
                    { 6178, false, 1, "São Roque" },
                    { 6171, false, 1, "São Miguel Arcanjo" },
                    { 5910, false, 1, "Nova Odessa" },
                    { 6279, false, 1, "Vinhedo" },
                    { 8858, true, 5, "Várzea Grande" },
                    { 76066, false, 1, "Chapecó" },
                    { 76414, true, 1, "Florianópolis" },
                    { 6096, false, 2, "Salto de Pirapora" },
                    { 6076, false, 2, "Rio Claro" },
                    { 6027, false, 2, "Praia Grande" },
                    { 5976, false, 2, "Pindamonhangaba" },
                    { 5972, false, 2, "Peruíbe" },
                    { 5966, false, 2, "Pedreira" },
                    { 6103, false, 2, "Santa Bárbara d Oeste" },
                    { 5923, false, 2, "Orlândia" },
                    { 5865, false, 2, "Mogi Mirim" },
                    { 5863, false, 2, "Mogi Guaçu" },
                    { 5856, false, 2, "Mogi das Cruzes" },
                    { 5850, false, 2, "Mirassol" },
                    { 5833, false, 2, "Mauá" },
                    { 5810, false, 2, "Mairinque" },
                    { 5869, false, 2, "Mongaguá" },
                    { 5791, false, 2, "Limeira" },
                    { 6104, false, 2, "Santa Branca" },
                    { 6129, false, 2, "Santo André" },
                    { 22, true, 3, "Porto Velho" },
                    { 89897, false, 2, "Santos ABC" },
                    { 88579, false, 2, "Serra Negra" },
                    { 51136, true, 2, "Recife" },
                    { 8756, false, 2, "Lucas do Rio Verde" },
                    { 6261, false, 2, "Ubatuba" },
                    { 6108, false, 2, "Santa Cruz do Rio Pardo" },
                    { 6235, false, 2, "Taubaté" },
                    { 6187, false, 2, "São Vicente" },
                    { 6182, false, 2, "São Sebastião" },
                    { 6162, false, 2, "São José dos Campos" },
                    { 6154, false, 2, "São Joaquim da Barra" },
                    { 6143, false, 2, "São Caetano do Sul" },
                    { 6141, false, 2, "São Bernardo do Campo" },
                    { 6207, false, 2, "Sumaré" },
                    { 76180, true, 1, "Criciúma" },
                    { 5761, false, 2, "Jaguariúna" },
                    { 5749, false, 2, "Itúverava" },
                    { 5405, false, 2, "Aracoiaba da Serra" },
                    { 5404, false, 2, "Araçatuba" },
                    { 5389, false, 2, "Amparo" },
                    { 5386, false, 2, "Americana" },
                    { 5268, true, 2, "Resende" },
                    { 5116, false, 2, "Armação dos Búzios" },
                    { 5409, false, 2, "Araraquara" },
                    { 5100, true, 2, "Barra Mansa" },
                    { 2291, true, 2, "Paulista" },
                    { 2269, true, 2, "Olinda" },
                    { 2235, false, 2, "Jaboatão dos Guararapes" },
                    { 156, false, 2, "Boa Vista" },
                    { 121, true, 2, "Manaus" },
                    { 77127, true, 1, "Joinville" },
                    { 4770, false, 2, "Timóteo" },
                    { 5756, false, 2, "Jacareí" },
                    { 5413, false, 2, "Araras" },
                    { 5442, false, 2, "Barrinha" },
                    { 5746, false, 2, "Itú" },
                    { 5715, false, 2, "Itapetininga" },
                    { 5712, false, 2, "Itanhaém" },
                    { 5668, false, 2, "Hortolândia" },
                    { 5659, false, 2, "Guarujá" },
                    { 5577, false, 2, "Diadema" },
                    { 5423, false, 2, "Atibaia" },
                    { 5576, false, 2, "Descalvado" },
                    { 5569, false, 2, "Cravinhos" },
                    { 5531, false, 2, "Caraguatatuba" },
                    { 5487, false, 2, "Caçapava" },
                    { 5474, false, 2, "Bragança Paulista" },
                    { 5464, false, 2, "Boituva" },
                    { 5456, false, 2, "Bertioga" },
                    { 5573, false, 2, "Cubatão" },
                    { 88412, false, 6, "São Paulo" }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "ChangePassword", "CityId", "Email", "Name", "Password", "Phone", "Prefix", "Username" },
                values: new object[,]
                {
                    { 1, false, 71986, "v.arre@akivasoftware.com.br", "Robo Serena", "serena@2019", 999999999, 51, "serena" },
                    { 2, true, 71986, "vagner.cardoso@net.com.br", "Gabriel Edson Lopes de Quadros", "HQtZTtyz", 991288568, 51, "N0139239" },
                    { 3, true, 71986, "vagner.cardoso@net.com.br", "Silvio Cesar de Oliveira", "0PHzc1Tj", 991207273, 51, "N0155180" },
                    { 4, true, 71986, "vagner.cardoso@net.com.br", "Sidimar Zanotelli", "ABowW8z6", 993930719, 51, "N5979438" },
                    { 5, true, 71986, "vagner.cardoso@net.com.br", "Leandro Peixoto Alves", "3vhGHrkC", 993667617, 51, "N5706752" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_City_ExternalSystemId",
                table: "City",
                column: "ExternalSystemId");

            migrationBuilder.CreateIndex(
                name: "IX_Outage_CityId",
                table: "Outage",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_Outage_ClosedById",
                table: "Outage",
                column: "ClosedById");

            migrationBuilder.CreateIndex(
                name: "IX_Outage_OpenedById",
                table: "Outage",
                column: "OpenedById");

            migrationBuilder.CreateIndex(
                name: "IX_Outage_ServiceOrderTypeId",
                table: "Outage",
                column: "ServiceOrderTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Outage_SymptomId",
                table: "Outage",
                column: "SymptomId");

            migrationBuilder.CreateIndex(
                name: "IX_Outage_TechnicUserId",
                table: "Outage",
                column: "TechnicUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Outage_TypeId",
                table: "Outage",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_OutageService_ServiceId",
                table: "OutageService",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_OutageSignal_OutageId",
                table: "OutageSignal",
                column: "OutageId");

            migrationBuilder.CreateIndex(
                name: "IX_OutageSignal_StatusId",
                table: "OutageSignal",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_OutageSignal_TerminalId",
                table: "OutageSignal",
                column: "TerminalId");

            migrationBuilder.CreateIndex(
                name: "IX_SignalCmRx_OutageSignalId",
                table: "SignalCmRx",
                column: "OutageSignalId");

            migrationBuilder.CreateIndex(
                name: "IX_SignalCmSnr_OutageSignalId",
                table: "SignalCmSnr",
                column: "OutageSignalId");

            migrationBuilder.CreateIndex(
                name: "IX_User_CityId",
                table: "User",
                column: "CityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Log");

            migrationBuilder.DropTable(
                name: "OutageService");

            migrationBuilder.DropTable(
                name: "Resolution");

            migrationBuilder.DropTable(
                name: "SignalCmRx");

            migrationBuilder.DropTable(
                name: "SignalCmSnr");

            migrationBuilder.DropTable(
                name: "WebCommLog");

            migrationBuilder.DropTable(
                name: "Service");

            migrationBuilder.DropTable(
                name: "OutageSignal");

            migrationBuilder.DropTable(
                name: "Outage");

            migrationBuilder.DropTable(
                name: "SignalStatus");

            migrationBuilder.DropTable(
                name: "SignalTerminal");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Username");

            migrationBuilder.DropTable(
                name: "ServiceOrderType");

            migrationBuilder.DropTable(
                name: "OutageSymptom");

            migrationBuilder.DropTable(
                name: "OutageType");

            migrationBuilder.DropTable(
                name: "City");

            migrationBuilder.DropTable(
                name: "ExternalSystem");
        }
    }
}
