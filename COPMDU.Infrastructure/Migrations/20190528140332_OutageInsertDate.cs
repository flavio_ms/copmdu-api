﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace COPMDU.Infrastructure.Migrations
{
    public partial class OutageInsertDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "InsertDate",
                table: "Outage",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "TechnicianUpdatedTime",
                table: "Outage",
                nullable: true);

            migrationBuilder.InsertData(
                table: "SignalStatus",
                columns: new[] { "Id", "Name" },
                values: new object[] { 10, "Failed" });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SignalStatus",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DropColumn(
                name: "TechnicianUpdatedTime",
                table: "Outage");

            migrationBuilder.DropColumn(
                name: "InsertDate",
                table: "Outage");
        }
    }
}
