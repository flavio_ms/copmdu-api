﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace COPMDU.Domain.Entity
{
    public class Tickets
    {
        public IEnumerable<TicketResponse> Data { get; set; }
    }


    [Serializable]
    [XmlRoot(ElementName = "QualiNET")]
    public class ServiceOrderCollection
    {
        [XmlElement("OrdSrv")]
        public List<ServiceOrder> ServiceOrders { get; set; }
    }
        

    [Serializable]
    [XmlRoot(ElementName = "OrdSrv")]
    public class ServiceOrder
    {
        [XmlAttribute(AttributeName = "DT_UPDATE")]
        public string DT_UPDATE { get; set; }
        [XmlAttribute(AttributeName = "LOG_VT")]
        public string LOG_VT { get; set; }
        [XmlAttribute(AttributeName = "GARANTIA")]
        public string GARANTIA { get; set; }
        [XmlAttribute(AttributeName = "USR_ATEND")]
        public string USR_ATEND { get; set; }
        [XmlAttribute(AttributeName = "TREM")]
        public string TREM { get; set; }
        [XmlAttribute(AttributeName = "DT_MAX_ATEND_PRAZO")]
        public string DT_MAX_ATEND_PRAZO { get; set; }
        [XmlAttribute(AttributeName = "COMPL1_DESCR")]
        public string COMPL1_DESCR { get; set; }
        [XmlAttribute(AttributeName = "ID_COMPL1")]
        public string ID_COMPL1 { get; set; }
        [XmlAttribute(AttributeName = "END_COMPLETO")]
        public string END_COMPLETO { get; set; }
        [XmlAttribute(AttributeName = "COD_IMOVEL")]
        public string COD_IMOVEL { get; set; }
        [XmlAttribute(AttributeName = "NUM_CEP")]
        public string NUM_CEP { get; set; }
        [XmlAttribute(AttributeName = "COD_NODE")]
        public string COD_NODE { get; set; }
        [XmlAttribute(AttributeName = "ID_REGIAO")]
        public string ID_REGIAO { get; set; }
        [XmlAttribute(AttributeName = "COD_HUB")]
        public string COD_HUB { get; set; }
        [XmlAttribute(AttributeName = "FN_CONVENIENCIA")]
        public string FN_CONVENIENCIA { get; set; }
        [XmlAttribute(AttributeName = "WO_EQUIPE_CELULAR")]
        public string WO_EQUIPE_CELULAR { get; set; }
        [XmlAttribute(AttributeName = "WO_EQUIPE_TECNICA")]
        public string WO_EQUIPE_TECNICA { get; set; }
        [XmlAttribute(AttributeName = "WO_EQUIPE_LOGIN")]
        public string WO_EQUIPE_LOGIN { get; set; }
        [XmlAttribute(AttributeName = "WO_STATUSDESC")]
        public string WO_STATUSDESC { get; set; }
        [XmlAttribute(AttributeName = "WO_SUBSTATUS")]
        public string WO_SUBSTATUS { get; set; }
        [XmlAttribute(AttributeName = "WO_JOBSTATUS")]
        public string WO_JOBSTATUS { get; set; }
        [XmlAttribute(AttributeName = "WO_STATUS_DESCRICAO")]
        public string WO_STATUS_DESCRICAO { get; set; }
        [XmlAttribute(AttributeName = "WO_STATUS")]
        public string WO_STATUS { get; set; }
        [XmlAttribute(AttributeName = "WO_ID")]
        public string WO_ID { get; set; }
        [XmlAttribute(AttributeName = "AREA_DESCRICAO")]
        public string AREA_DESCRICAO { get; set; }
        [XmlAttribute(AttributeName = "TEL_COM")]
        public string TEL_COM { get; set; }
        [XmlAttribute(AttributeName = "TEL_CEL")]
        public string TEL_CEL { get; set; }
        [XmlAttribute(AttributeName = "TEL_RES")]
        public string TEL_RES { get; set; }
        [XmlAttribute(AttributeName = "AGENDA_DESCR")]
        public string AGENDA_DESCR { get; set; }
        [XmlAttribute(AttributeName = "DT_AGENDA")]
        public string DT_AGENDA { get; set; }
        [XmlAttribute(AttributeName = "DT_CADASTRO")]
        public string DT_CADASTRO { get; set; }
        [XmlAttribute(AttributeName = "OS_RESUMO")]
        public string OS_RESUMO { get; set; }
        [XmlAttribute(AttributeName = "SEGMENTO_DESCR")]
        public string SEGMENTO_DESCR { get; set; }
        [XmlAttribute(AttributeName = "NUM_CONTRATO")]
        public long NUM_CONTRATO { get; set; }
        [XmlAttribute(AttributeName = "ID_TIPO_OS")]
        public string ID_TIPO_OS { get; set; }
        [XmlAttribute(AttributeName = "COD_OS")]
        public string COD_OS { get; set; }
        [XmlAttribute(AttributeName = "COD_OPERADORA")]
        public string COD_OPERADORA { get; set; }
        [XmlAttribute(AttributeName = "CID_CONTRATO")]
        public string CID_CONTRATO { get; set; }
    }

    [XmlRoot(ElementName = "QualiNET")]
    public class QualiNet
    {
        [XmlElement(ElementName = "OrdSrv")]
        public List<ServiceOrder> OrdSrv { get; set; }
    }
}
